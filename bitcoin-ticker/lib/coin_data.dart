import 'package:http/http.dart' as http;
import 'dart:convert';


const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

const coinAPIURL = 'https://rest.coinapi.io/v1/exchangerate';
const apiKey = '6B8E2B66-14E7-4F65-B67E-B1127588922B';

class CoinData {

  Future getCoinData() async{

    String requestURL = '$coinAPIURL/BTC/USD?apikey=$apiKey';
    http.Response response = await http.get(requestURL);

    if (response.statusCode == 200) {

      var jsonRes = jsonDecode(response.body)['rate'];
      int jsonResponse = jsonRes.toInt();

      print(jsonResponse);

      return jsonResponse;

    } else {
      print(response.statusCode);
    }
  }
}
